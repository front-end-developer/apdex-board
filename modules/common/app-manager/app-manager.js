/**
 * Created by Mark Webley on 06/04/2020.
 */
export class AppManager {

    /**
     * call all onDestroy Methods for platform clean up
     * @description     can be abstract,
     *                  intentionally not implemented for this test.
     */
    onDestroy() {

    }
}
