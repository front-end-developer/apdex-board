/**
 * Created by Mark Webley on 05/04/2020.
 */
import Template from "../template.js";
import Dashboard from "../dashboard.js";

describe('Dashboard', () => {
    const mockHosts = [
        "7e6272f7-098e.dakota.biz",
        "9a450527-cdd9.kareem.info",
        "e7bf58af-f0be.dallas.biz"
    ];

    const mockHost = [
        "7e6272f7-098e.dakota.biz"
    ];

    const mockAppData = {
        "name": "Small Fresh Pants - Kautzer - Boyer, and Sons",
        "contributors": [
            "Edwin Reinger",
            "Ofelia Dickens",
            "Hilbert Cole",
            "Helen Kuphal",
            "Maurine McDermott Sr."
        ],
        "version": 7,
        "apdex": 68,
        "host": [
            "7e6272f7-098e.dakota.biz",
            "9a450527-cdd9.kareem.info",
            "e7bf58af-f0be.dallas.biz"
        ]
    };

    const mockAppTransformed = {
        "name": "Awesome Wooden Sausages - Schaefer - Hegmann, Inc",
        "contributors": [
            "Karson Marquardt III",
            "Lydia Lind",
            "Abel Ratke",
            "Aaron Nicolas",
            "Martin Weber"
        ],
        "version": 7,
        "apdex": 100
    };

    const mockTemplateData = {
        "host": "e7bf58af-f0be.dallas.biz",
        "apps": [
            {
                "name": "Practical Fresh Chips - Weber - Lemke, Inc",
                "contributors": [
                    "Mr. Jarvis Hammes",
                    "Rey Reichert"
                ],
                "version": 4,
                "apdex": 99
            },
            {
                "name": "Sleek Frozen Pants - Heaney - Heathcote, and Sons",
                "contributors": [
                    "Orlando Prosacco II",
                    "Tito Wyman"
                ],
                "version": 4,
                "apdex": 98
            },
            {
                "name": "Awesome Wooden Sausages - Schaefer - Hegmann, Inc",
                "contributors": [
                    "Karson Marquardt III",
                    "Lydia Lind",
                    "Abel Ratke",
                    "Aaron Nicolas",
                    "Martin Weber"
                ],
                "version": 7,
                "apdex": 100
            },
            {
                "name": "Gorgeous Frozen Gloves - D'Amore, Hilpert and Macejkovic, LLC",
                "contributors": [
                    "Kaia Lubowitz",
                    "Rodrick Roberts",
                    "Jose Ernser"
                ],
                "version": 6,
                "apdex": 98
            },
            {
                "name": "Fantastic Cotton Soap - Langosh - O'Reilly, and Sons",
                "contributors": [
                    "Winfield Cronin",
                    "Anabelle Ruecker",
                    "Nina Welch",
                    "Destinee Ratke",
                    "Rey Jacobs"
                ],
                "version": 9,
                "apdex": 97
            },
            {
                "name": "Ergonomic Granite Chair - Buckridge - Hackett, LLC",
                "contributors": [
                    "Aliza Bins",
                    "Fleta Pouros",
                    "Annette Hintz"
                ],
                "version": 2,
                "apdex": 96
            },
            {
                "name": "Unbranded Metal Shoes - Miller, Blick and Goyette, Group",
                "contributors": [
                    "Lowell Brakus",
                    "Adonis Erdman",
                    "Bryana Gaylord"
                ],
                "version": 4,
                "apdex": 96
            },
            {
                "name": "Intelligent Metal Keyboard - O'Kon, Wehner and Ritchie, Group",
                "contributors": [
                    "August Reynolds",
                    "Corine Dicki DVM",
                    "Johan Collins"
                ],
                "version": 1,
                "apdex": 96
            },
            {
                "name": "Handcrafted Granite Tuna - Smith, Schroeder and Mosciski, Group",
                "contributors": [
                    "Allie Harris",
                    "Kobe Jaskolski",
                    "Minerva Monahan"
                ],
                "version": 3,
                "apdex": 93
            },
            {
                "name": "Practical Granite Hat - Smith LLC, Inc",
                "contributors": [
                    "Rey Blick"
                ],
                "version": 2,
                "apdex": 93
            },
            {
                "name": "Awesome Frozen Keyboard - Parker - Daugherty, Inc",
                "contributors": [
                    "Miss Mittie Gutkowski",
                    "Eden Upton DVM"
                ],
                "version": 1,
                "apdex": 93
            },
            {
                "name": "Gorgeous Plastic Computer - Skiles, Little and Legros, and Sons",
                "contributors": [
                    "Cloyd Bernier"
                ],
                "version": 7,
                "apdex": 93
            },
            {
                "name": "Handcrafted Steel Fish - Grant, Jenkins and Kunze, Inc",
                "contributors": [
                    "Stanton Schneider",
                    "Hettie Wintheiser",
                    "Monte Mohr"
                ],
                "version": 9,
                "apdex": 93
            },
            {
                "name": "Unbranded Frozen Towels - Brakus, Gleichner and Bauch, Inc",
                "contributors": [
                    "Jasmin Kulas I",
                    "Jayce Stiedemann"
                ],
                "version": 1,
                "apdex": 92
            },
            {
                "name": "Refined Granite Soap - Bergstrom, Bins and Dibbert, LLC",
                "contributors": [
                    "Lottie Pouros",
                    "Ashlynn Collier",
                    "Mrs. Christ Frami",
                    "Carey Langworth"
                ],
                "version": 5,
                "apdex": 92
            },
            {
                "name": "Gorgeous Plastic Cheese - Pfannerstill - Beahan, LLC",
                "contributors": [
                    "Orlando Schuster",
                    "Mrs. Thad Kuvalis",
                    "David Lueilwitz",
                    "Bud Nikolaus DVM"
                ],
                "version": 9,
                "apdex": 92
            },
            {
                "name": "Handcrafted Soft Chips - D'Amore - Moen, Group",
                "contributors": [
                    "Elias Thiel"
                ],
                "version": 3,
                "apdex": 92
            },
            {
                "name": "Handmade Fresh Gloves - Botsford - Cassin, LLC",
                "contributors": [
                    "Annette Larkin I"
                ],
                "version": 5,
                "apdex": 92
            },
            {
                "name": "Fantastic Steel Soap - Langosh, Daniel and West, LLC",
                "contributors": [
                    "Adolf Mante",
                    "Idell Spencer DDS",
                    "Taya Stanton"
                ],
                "version": 10,
                "apdex": 91
            },
            {
                "name": "Handcrafted Fresh Towels - Spencer LLC, Group",
                "contributors": [
                    "Hayley Boehm",
                    "Hardy Abbott"
                ],
                "version": 10,
                "apdex": 91
            },
            {
                "name": "Rustic Concrete Chicken - Botsford - Grady, LLC",
                "contributors": [
                    "Eryn Pfeffer",
                    "Roel Runolfsson I"
                ],
                "version": 8,
                "apdex": 91
            },
            {
                "name": "Licensed Wooden Ball - Kemmer - Konopelski, LLC",
                "contributors": [
                    "Lee Mills",
                    "Destiny Friesen",
                    "Jamil Balistreri"
                ],
                "version": 10,
                "apdex": 91
            },
            {
                "name": "Ergonomic Plastic Salad - Bailey, Berge and Paucek, Group",
                "contributors": [
                    "Elliott Wiegand",
                    "Halie Windler"
                ],
                "version": 10,
                "apdex": 90
            },
            {
                "name": "Fantastic Soft Hat - Wiza, Adams and Mohr, Group",
                "contributors": [
                    "Rubie Gleichner",
                    "Shanel Braun",
                    "Electa Ondricka",
                    "Justina Kreiger"
                ],
                "version": 7,
                "apdex": 90
            },
            {
                "name": "Sleek Steel Ball - Friesen Inc, Group",
                "contributors": [
                    "Brian Smitham V",
                    "Pansy Kautzer",
                    "Caesar Ward"
                ],
                "version": 5,
                "apdex": 90
            }
        ]
    };

    const mockData = [
        {
            "name": "1) Mark Webley - Kautzer - Boyer, and Sons",
            "contributors": [
                "Edwin Reinger",
                "Ofelia Dickens",
                "Hilbert Cole",
                "Helen Kuphal",
                "Maurine McDermott Sr."
            ],
            "version": 7,
            "apdex": 68,
            "host": [
                "7e6272f7-098e.dakota.biz",
                "9a450527-cdd9.kareem.info",
                "e7bf58af-f0be.dallas.biz"
            ]
        },
        {
            "name": "2) Mark Webley - Kautzer - Boyer, and Sons",
            "contributors": [
                "Edwin Reinger",
                "Ofelia Dickens",
                "Hilbert Cole",
                "Helen Kuphal",
                "Maurine McDermott Sr."
            ],
            "version": 7,
            "apdex": 68,
            "host": [
                "7e6272f7-098e.dakota.biz",
                "9a450527-cdd9.kareem.info",
                "e7bf58af-f0be.dallas.biz"
            ]
        },
        {
            "name": "Small Fresh Pants - Kautzer - Boyer, and Sons",
            "contributors": [
                "Edwin Reinger",
                "Ofelia Dickens",
                "Hilbert Cole",
                "Helen Kuphal",
                "Maurine McDermott Sr."
            ],
            "version": 7,
            "apdex": 68,
            "host": [
                "7e6272f7-098e.dakota.biz",
                "9a450527-cdd9.kareem.info",
                "e7bf58af-f0be.dallas.biz"
            ]
        },
        {
            "name": "Mark Webley - Kautzer - Boyer, and Sons",
            "contributors": [
                "Edwin Reinger",
                "Ofelia Dickens",
                "Hilbert Cole",
                "Helen Kuphal",
                "Maurine McDermott Sr."
            ],
            "version": 7,
            "apdex": 68,
            "host": [
                "7e6272f7-098e.dakota.biz",
                "9a450527-cdd9.kareem.info",
                "e7bf58af-f0be.dallas.biz"
            ]
        },
        {
            "name": "Refined Concrete Shirt - Hudson - Sauer, Group",
            "contributors": [
                "Ramon Harris DDS",
                "Summer Dicki",
                "Triston Sipes",
                "Fae Lind",
                "Carole Emard"
            ],
            "version": 6,
            "apdex": 57,
            "host": [
                "e0419f48-6a5a.craig.info",
                "7e6272f7-098e.dakota.biz"
            ]
        },
        {
            "name": "Ergonomic Wooden Soap - Lemke and Sons, Inc",
            "contributors": [
                "Miss Moises Walter",
                "Caroline Murazik"
            ],
            "version": 2,
            "apdex": 61,
            "host": [
                "e7bf58af-f0be.dallas.biz",
                "b0b655c5-928a.nadia.biz",
                "95b346a0-17f4.abbigail.name"
            ]
        },
        {
            "name": "Mark Webley - example - webley and sons",
            "contributors": [
                "Edwin Reinger",
                "Ofelia Dickens",
                "Hilbert Cole",
                "Helen Kuphal",
                "Maurine McDermott Sr."
            ],
            "version": 7,
            "apdex": 100,
            "host": [
                "7e6272f7-098e.dakota.biz",
                "9a450527-cdd9.kareem.info",
                "e7bf58af-f0be.dallas.biz"
            ]
        },
    ];
    let dashboard;
    beforeEach(() => {
        dashboard = new Dashboard();
    });

    it('should createHost', () => {
        const dboard = dashboard.createHost(mockHost, mockAppData);
        // expect(dboard.createHostApp).toHaveBeenCalled();
        expect(dboard.createHostApp).toHaveBeenCalledWith(mockHost[0], mockAppData);
    });

    it('should createHostApp', () => {
        const dboard = dashboard.createHostApp(e7bf58af-f0be.dallas.biz, mockAppData);
        expect(dboard.createHostApp).toHaveBeenCalledWith(mockHost[0], mockAppData);
        expect(dboard.hostApplications[0].host).toContain(mockHost[0]);
        expect(dboard.hostApplications.length).toEqual(1);
    });

    it('should drawPanelTemplate', () => {
        const testTemplate = '<div>test</div>';
        const testApp = {
            app: {
                host: mockHost
            }
        };
        const dboard = dashboard.drawPanelTemplate(mockTemplateData);
        const spy = spyOn(Template, 'panel');
        Template.panel(testApp, testTemplate);
        expect(spy).toHaveBeenCalled();
    });

    it('should drawAppTemplate', () => {
        const dboard = dashboard.drawAppTemplate(mockAppTransformed);
        let spy = spyOn(Template, 'apps');
        Template.apps(mockAppTransformed);
        expect(spy).toHaveBeenCalled();
    });

    it('should render', () => {
        const dboard = dashboard.render();
        expect(dboard.hostApplications.length).toBeGreaterThan(1);
    });
});
