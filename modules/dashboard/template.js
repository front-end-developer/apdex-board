/**
 * Created by Mark Webley on 05/04/2020.
 */
export default class Template {

    static panel(tmpl, appsTemplate) {
        return `
            <div class="panel">
                <section>
                    <h3>${tmpl.host}</h3>
                     ${appsTemplate}
                 </section>
            </div>
            `;
    }

    static apps(app) {
        return `
        <div class="host-application" data-app="${app.version}">
            <span class="app-version-number">Version: ${app.version}</span>
            <span class="apdex">${app.apdex}</span>
            <span class="apdex-name">${app.name}</span>
        </div>`;
    }
}
