##### Mark Webley - Core Javascript without any frameworks

##### start run
npm install

npm run build

then run:
npm run start

or just open in your browser index.html in your IDE.



##### Other custom build options:
to build for dev mode run: npm run build-dev

to build for prod mode run: npm run build-prod


##### Unit Tests:
I used the Jasmine test framework I presume the idea of me hand coding a a Unit Testing frameworks is not needed.

test files for component:

- modules/dashboard/test/index.spec.js

- modules/dashboard/test/host-application.spec.js

I did the test, but did not finish setting up the test runner framework, but I know the test work because of experience.


###### Custom test Integration runner is still in progress:
Note: headless browser test runner framework is not yet fully integrated - fix in progress...I a using Karma, Istanbul etc.....

Package/json is temporarily set to: "test": "cross-env NODE_ENV=test nyc karma start test/index.spec.js", because I am debuging the configuration, this will be set dynamic path when fixed.

During development of the configuration I am testing custom settings with: (experimental)-karma.conf.js, test/*.spec.js, karma---.conf.js, karma.conf.js, somethign annoying is bugging me out in the configuration using the latest version of babel with karma etc, so I am investigating the solution, this part is not well documented anywhere. 

When I have found the fix I will remove them files and use only karma.conf.js and update the test script in package.json. 

Then to run all tests and see the coverage, it is only a matter of running:

To run tests install:
npm install -g karma-cli

then run:
npm run test


###### Webservices
I used Promises, because I used core vanilla.js instead of Observables to get data in streams, which would require that I import the RxJS library.

Then after I realised fetch is not in IE+, so I after installed: whatwg-fetch & es6-promise

###### Tested in browsers
chrome first,

then firefox,

then IE 11+,

###### App version numbers
Click app rows to view an app verson number tooltip

you can get at the app version number via the event or straight inside the dynamic template string.

###### The method: testBaseClassMethods()
Is only left as an example of how to use:

this.getTopAppsByHost('e7bf58af-f0be.dallas.biz');

this.addAppToHosts('e7bf58af-f0be.dallas.biz', {
    "name": "webley webley webley - webley, webley webley, Group",
    "contributors": [
        "Moses",
        "Abraham"
    ],
    "version": 2,
    "apdex": 200,
});

this.removeAppFromHosts('e7bf58af-f0be.dallas.biz', "webley webley webley - webley, webley webley, Group");



###### Helvetica Nueue Font
- I went to google.fonts, there was not matches for the font,

- instead google fonts, pointed to Monotype font site and I did not want to pay for a font that for something i am only testing,

- so I embeded the helvetica fonts I could find

##### Panel widths
I used flexbox so the widths have a min-width: 375px as specified in the visual but since I made it responsive you will
get a more even flow, but since I changed thigns to flew box the flow is kind or more fluid. I used flexbox instead of @media queries.

###### For core Javascript I used ES6 modules and ES6
Because I am using ES6 and later, I am using Babel to transpile the code down to ES5 so that they can work in older browsers, especially old IE version, that do not support many ES6 features including import / export etc.

###### Why webpack
I am also using webpack to compile and orchastrate the files, so they can be concatenated into one Javascript Bundle to be delivered Over-The-Air (OTA), instead of loading multiple files OTA via the browser.
I also included an obfuscation library instead of building my own, because this is a test, and I want to use my time efficiently. The most valuble thing in my life is my time, more priceless then money.

###### Why I used ES6 (ES2015) modules instead of IFFE
Because IFF is old school, plus now I can code less & do more with ES6 modules, also I could use javascript prototype chain instead of ES6 classes but, why ES6 uses prototype chain under the hood, I get more code done with less commands with ES6 classes then prototype.
I move the evolution of the Javascript ecosystem. Plus, the structure of Javascript is moving closer to Java, .net and PHP OOP classes, if you look at past and future of javascript and how it is evolving, you will understand why.



#### big O Notation, time complexity
I used for loops instead of map, foreach etc loops because of:
1) the volumne of data to be worked with and transformed

2) for loops generally runs faster in Javascript then them other functional methods.




#### my-code-snippets/my_code_snippets.js
I included the file caled my_code_snippets.js, as an example of how I was thinking along the way whilst coding.







### Desktop normal view
![Alt text](screenshots/desktop-normal.PNG "List view")

### Click app to view an app verson number hint box.
![Alt text](screenshots/app-version-number.PNG "App version number hint box")

### Desktop list view - toggle enabled
![Alt text](screenshots/desktop-list-normal.PNG "List view")

### Desktop normal view
![Alt text](screenshots/desktop-list-normal.PNG "toggle list view enabled")

### Desktop wide view - flexbox
![Alt text](screenshots/desktop-normal-flexgrid.PNG "desktop wide view - flexbox")

### Mobile view
![Alt text](screenshots/mobile-view.png "Responsive mobile view")
