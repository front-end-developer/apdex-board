import {AppManager} from "../common/app-manager/app-manager.js";

/**
 * Created by Mark Webley on 05/04/2020.
 */
export default class HostApplications extends AppManager {
    hostApplications = [];
    appProperties = {
        name: 'name',
        contributors: 'contributors',
        version: 'version',
        apdex: 'apdex'
    };

    constructor(hostApplications = null) {
        super();
        if (hostApplications !== null && hostApplications.length > 0) {
            this.hostApplications = hostApplications;
        }
    }

    validateHostName(hostName) {
        var regExp = new RegExp('^([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?');
        return regExp.test(hostName);
    }

    validateAppObject(app) {
        let valid = true;
        const errorMessage = 'Error: app object is missing ';
        if (typeof app === 'undefined' || app === null) {
            throw new Error('app data not provided');
        }

        if (Object.keys(app).indexOf(this.appProperties.name) == -1) {
            valid = false;
            throw new Error(`${errorMessage} ${this.appProperties.name}`);
        }
        if (Object.keys(app).indexOf(this.appProperties.contributors) == -1) {
            valid = false;
            throw new Error(`${errorMessage} ${this.appProperties.contributors}`);
        }
        if (Object.keys(app).indexOf(this.appProperties.version) == -1) {
            valid = false;
            throw new Error(`${errorMessage} ${this.appProperties.version}`);
        }
        if (Object.keys(app).indexOf(this.appProperties.apdex) == -1) {
            valid = false;
            throw new Error(`${errorMessage} ${this.appProperties.apdex}`);
        }
        return valid;
    }

    /**
     * @description    method that, given a hostname, retrieves a
     *                  list of the 25 most satisfying applications.
     */
    getTopAppsByHost(hostName) {
        if (!this.validateHostName(hostName)) {
            throw new Error('invalid hostname');
            return;
        }
        let topItems = '';
        let numberOfApps = 0;
        let maxNumberOfApps = 25;
        const hostObject = this.hostApplications.find((domain) => domain.host === hostName);
        hostObject.apps.sort((prev, next) => {
            return next.apdex - prev.apdex;
        });

        const copyHostObject = JSON.parse(JSON.stringify(hostObject));
        copyHostObject.apps.splice(25);
        return copyHostObject;
    };

    /**
     * @hostName        hostname
     * @appData         {
         *                      "name": "Ergonomic Wooden Soap - Lemke and Sons, Inc",
         *                      "contributors": [
         *                           "Miss Moises Walter",
         *                           "Caroline Murazik"
         *                        ],
         *                       "version": 2,
         *                       "apdex": 61,
         *                    }
     * @description    update the list of applications with higher Apdex
     */
    addAppToHosts(hostName, appData) {
        if (!this.validateAppObject(appData)) {
            return;
        }
        if (!this.validateHostName(hostName)) {
            throw new Error('invalid hostname');
            return;
        }
        this.hostApplications.find((domain) => domain.host === hostName).apps.push(appData);
        this.getTopAppsByHost(hostName);
    };

    /**
     * @description    update the list of applications with higher Apdex
     */
    removeAppFromHosts(hostName, appName) {
        if (!this.validateHostName(hostName)) {
            throw new Error('invalid hostname');
            return;
        }
        if (typeof appName === 'undefined' || appName === null) {
            throw new Error('invalid app name');
            return;
        }

        const hostApp = this.hostApplications.find((domain) => domain.host === hostName);
        let index = 0;
        for (let app of hostApp.apps) {
            if (app.name === appName) {
                delete hostApp.apps[index];
                break;
            }
            index++;
        }
        this.getTopAppsByHost(hostName);
    };
}
