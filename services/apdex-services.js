/**
 * Created by Mark Webley on 04/04/2020.
 * Singleton service
 */

// TODO: why does this Module break when I run Karma, Istanbal???? (ReferenceError: regeneratorRuntime is not defined)

class API {
    static HOSTS = 'mocks/host-app-data.json';
}

export default class ApdexServices {
    static catche = null;
    static instance;
    constructor() {
        if (typeof this.instance === 'undefined') {
            throw new Error('use ApdexServices.getInstance() of singleton');
        } else {
            return this.instance;
        }
    }

    // TODO: use parent class or compositon over inheritance
    static toggleLoader(state = true) {
        // I intentionally left code out in this sample, is to show a in the ui when loading services
    }

    static getInstance() {
        if (typeof this.instance === 'undefined') {
            return this.instance = this;
        } else {
            return this.instance;
        }
    }

    static async getAll() {
        if (ApdexServices.catche === null) {
            ApdexServices.toggleLoader(true);
            try {
                const res = await fetch(API.HOSTS);
                ApdexServices.catche =
                    await res.json();
                return ApdexServices.catche;
            } catch(err) {
                console.error('Error: on save: ', err.message);
                return null;
            } finally {
                ApdexServices.toggleLoader(false);
            }
        } else {
            return ApdexServices.catche;
        }
    }
}
