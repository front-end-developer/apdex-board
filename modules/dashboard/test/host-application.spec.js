/**
 * Created by Mark Webley on 05/04/2020.
 */
import HostApplications from "../host-applications.js";

describe('HostApplications', () => {

    const mockHostName = 'e7bf58af-f0be.dallas.biz';
    const mockAppInsertion = {
        "name": "webley webley webley - webley, webley webley, Group",
        "contributors": [
            "Moses",
            "Abraham"
        ],
        "version": 2,
        "apdex": 200,
    };
    const mockData = [
        {
            "name": "1) Mark Webley - Kautzer - Boyer, and Sons",
            "contributors": [
                "Edwin Reinger",
                "Ofelia Dickens",
                "Hilbert Cole",
                "Helen Kuphal",
                "Maurine McDermott Sr."
            ],
            "version": 7,
            "apdex": 68,
            "host": [
                "7e6272f7-098e.dakota.biz",
                "9a450527-cdd9.kareem.info",
                "e7bf58af-f0be.dallas.biz"
            ]
        },
        {
            "name": "2) Mark Webley - Kautzer - Boyer, and Sons",
            "contributors": [
                "Edwin Reinger",
                "Ofelia Dickens",
                "Hilbert Cole",
                "Helen Kuphal",
                "Maurine McDermott Sr."
            ],
            "version": 7,
            "apdex": 68,
            "host": [
                "7e6272f7-098e.dakota.biz",
                "9a450527-cdd9.kareem.info",
                "e7bf58af-f0be.dallas.biz"
            ]
        },
        {
            "name": "Small Fresh Pants - Kautzer - Boyer, and Sons",
            "contributors": [
                "Edwin Reinger",
                "Ofelia Dickens",
                "Hilbert Cole",
                "Helen Kuphal",
                "Maurine McDermott Sr."
            ],
            "version": 7,
            "apdex": 68,
            "host": [
                "7e6272f7-098e.dakota.biz",
                "9a450527-cdd9.kareem.info",
                "e7bf58af-f0be.dallas.biz"
            ]
        },
        {
            "name": "Mark Webley - Kautzer - Boyer, and Sons",
            "contributors": [
                "Edwin Reinger",
                "Ofelia Dickens",
                "Hilbert Cole",
                "Helen Kuphal",
                "Maurine McDermott Sr."
            ],
            "version": 7,
            "apdex": 68,
            "host": [
                "7e6272f7-098e.dakota.biz",
                "9a450527-cdd9.kareem.info",
                "e7bf58af-f0be.dallas.biz"
            ]
        },
        {
            "name": "Refined Concrete Shirt - Hudson - Sauer, Group",
            "contributors": [
                "Ramon Harris DDS",
                "Summer Dicki",
                "Triston Sipes",
                "Fae Lind",
                "Carole Emard"
            ],
            "version": 6,
            "apdex": 57,
            "host": [
                "e0419f48-6a5a.craig.info",
                "7e6272f7-098e.dakota.biz"
            ]
        },
        {
            "name": "Ergonomic Wooden Soap - Lemke and Sons, Inc",
            "contributors": [
                "Miss Moises Walter",
                "Caroline Murazik"
            ],
            "version": 2,
            "apdex": 61,
            "host": [
                "e7bf58af-f0be.dallas.biz",
                "b0b655c5-928a.nadia.biz",
                "95b346a0-17f4.abbigail.name"
            ]
        },
        {
            "name": "Mark Webley - example - webley and sons",
            "contributors": [
                "Edwin Reinger",
                "Ofelia Dickens",
                "Hilbert Cole",
                "Helen Kuphal",
                "Maurine McDermott Sr."
            ],
            "version": 7,
            "apdex": 100,
            "host": [
                "7e6272f7-098e.dakota.biz",
                "9a450527-cdd9.kareem.info",
                "e7bf58af-f0be.dallas.biz"
            ]
        },
    ];
    let hostApplication;
    beforeEach(() => {
        hostApplication = new HostApplications(mockData);
    });

    it('should getTopAppsByHost', () => {
        const topTwentyFive = hostApplication.getTopAppsByHost(mockHostName);
        expect(topTwentyFive[0].name).toEqual('Mark Webley - example - webley and sons');
    });

    it('should addAppToHosts', () => {
        const topTwentyFive = hostApplication.addAppToHosts(mockHostName, mockAppInsertion);
        expect(topTwentyFive[0].name).toEqual('webley webley webley - webley, webley webley, Group');
    });

    it('should removeAppFromHosts', () => {
        hostApplication.addAppToHosts(mockHostName, mockAppInsertion);
        const topTwentyFive = hostApplication.removeAppFromHosts('e7bf58af-f0be.dallas.biz', mockAppInsertion.name);
        expect(topTwentyFive[0].name).not.toContain('webley webley webley - webley, webley webley, Group');
    });
});
