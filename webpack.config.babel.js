/**
 * Created by Mark Webley on 03/04/2020.
 */
import {resolve} from "path";
import WebpackUglifyJsPlugin from 'webpack-uglify-js-plugin';
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const isDevelopment = (process.env.NODE_ENV == 'development') ? true : false;
require("es6-promise").polyfill();

module.exports = {
    entry: {
        'mark-webley-app': [
            '@babel/polyfill',
            'whatwg-fetch', // fetch API ployfil for IE
            './index.js'
        ]
    },
    // target: 'web',
    output: {
        path: resolve(__dirname + '/build/'),
        filename: 'bundle.[name].min.js',
        sourceMapFilename: '[name].map',
    },
    devServer: {
        port: 3200
    },
    devtool: isDevelopment ? 'eval-source-map' : 'none',
    resolve: {
        extensions: ['.js', 'scss', '.json', '.html'],
    },
    stats: {
        colors: true,
        reasons: true,
        chunks: true
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: (isDevelopment ? 'none' : 'babel-loader')
                }
            },
            {
                test: /\.css$/,
                exclude: [
                    /node_modules/
                ],
                use: [
                    {
                        loader: isDevelopment ? 'style-loader' : MiniCssExtractPlugin.loader,
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            //url: false,
                            sourceMap: isDevelopment
                        }
                    }
                ]
            },
            {
                test: /\.module\.scss$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: isDevelopment ? 'style-loader' : MiniCssExtractPlugin.loader,
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            modules: {
                                mode: 'local',
                                localIdentName: '[name]__[local]__[hash:base64:5]',
                                hashPrefix: 'markwebley_',
                            },
                            // Don't use css-loader's automatic URL transforms
                            //url: false,
                            sourceMap: isDevelopment,
                            importLoaders: 1  // because there is only one loader before this one, insert number of loaders before this one
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: isDevelopment
                        }
                    }
                ]

            },
            {
                test: /\.scss$/,
                exclude: [
                    /\.module\.scss$/,
                    /node_modules/
                ],
                use: [
                    {
                        loader: isDevelopment ? 'style-loader' : MiniCssExtractPlugin.loader,
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            //url: false,
                            sourceMap: isDevelopment
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: isDevelopment
                        }
                    }
                ]

            },

        ]
    },

    plugins: [

       // new HtmlWebPackPlugin({
        //    template: "./index.html",
       //     filename: "./index.html"
       // }),

        new MiniCssExtractPlugin({
            filename: isDevelopment ? 'styles.[name].css' : 'styles.[name].min.css',
            chunkFilename: isDevelopment ? 'styles.[id].css' : 'styles.[id].[hash].css'
        }),

        new WebpackUglifyJsPlugin({
            cacheFolder: resolve(__dirname, 'build/'),
            debug: true,
            minimize: false,
            sourceMap: true,
            output: {
                comments: isDevelopment
            },
            compressor: {
                warnings: false,
                keep_fnames: isDevelopment
            },
            mangle: {
                keep_fnames: isDevelopment
            }
        })
    ]
};
