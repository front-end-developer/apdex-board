/**
 * Created by Mark Webley on 03/04/2020.
 */
import Dashboard from './modules/dashboard/dashboard.js';
import {Header} from './modules/header/header.js';

class App {
    constructor() {
        new Header();
        new Dashboard();
    }
}

new App();
