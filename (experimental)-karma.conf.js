/**
 * Created by Mark Webley on 07/04/2020.
 */
const path = require('path');

module.exports = function(config) {
    config.set({
        // ... rest of karma config

        // anything named karma-* is normally auto included so you probably dont need this
        plugins: [
            'karma-jasmine',
            'karma-jasmine-html-reporter',
            'karma-chrome-launcher',
            'karma-coverage-istanbul-reporter',
        ],

        client: {
            clearContext: false // leave Jasmine Spec Runner output visible in browser
        },

        reporters: ['coverage-istanbul'],

        restartOnFileChange: true,

        frameworks: ['jasmine'],

        files: [
            '**/**/test/*.spec.js'
            // modules/dashboard/test/host-application.spec.js
        ],

        // any of these options are valid: https://github.com/istanbuljs/istanbuljs/blob/aae256fb8b9a3d19414dcf069c592e88712c32c6/packages/istanbul-api/lib/config.js#L33-L39
        coverageIstanbulReporter: {
            // reports can be any that are listed here: https://github.com/istanbuljs/istanbuljs/tree/aae256fb8b9a3d19414dcf069c592e88712c32c6/packages/istanbul-reports/lib
            reports: ['json-summary', 'html', 'lcovonly'], //['html', 'lcovonly', 'text-summary'],

            // base output directory. If you include %browser% in the path it will be replaced with the karma browser name
            dir: path.join(__dirname, 'coverage/mark-webley-app'),

            // Combines coverage information from multiple browsers into one report rather than outputting a report
            // for each browser.
            combineBrowserReports: true,

            // if using webpack and pre-loaders, work around webpack breaking the source path
            fixWebpackSourcePaths: true,

            // Omit files with no statements, no functions and no branches from the report
            // skipFilesWithNoCoverage: true,

            // Most reporters accept additional config options. You can pass these through the `report-config` option
            'report-config': {
                // all options available at: https://github.com/istanbuljs/istanbuljs/blob/aae256fb8b9a3d19414dcf069c592e88712c32c6/packages/istanbul-reports/lib/html/index.js#L135-L137
                html: {
                    // outputs the report in ./coverage/html
                    subdir: 'html'
                }
            },

            // enforce percentage thresholds
            // anything under these percentages will cause karma to fail with an exit code of 1 if not running in watch mode
            thresholds: {
                emitWarning: true, // set to `true` to not fail the test command when thresholds are not met
                // thresholds for all files
                global: {
                    statements: 80,
                    lines: 80,
                    branches: 75,
                    functions: 80
                }
            },

            verbose: true, // output config used by istanbul for debugging

            // `instrumentation` is used to configure Istanbul API package.
            instrumentation: {
                // To include `node_modules` code in the report.
                'default-excludes': false
            }
        }
    });
};
