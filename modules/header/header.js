/**
 * Created by Mark Webley on 06/04/2020.
 */
import {AppManager} from "../common/app-manager/app-manager.js";

export class Header extends AppManager {
    checkbox;
    constructor() {
        super();
        this.addEvents();
    }

    toggleListStyle(e) {
        e.stopPropagation();
        this.doList();
    }

    doList() {
        const checkMarkLabel = {
            'list' : 'Show as list',
            'grid' : 'Show as an awesome grid'
        }
        const myNodeList = document.querySelectorAll('.panel');
        const checkMarkTitle = document.querySelector('.checkmark-title');
        for (var i = 0; i < myNodeList.length; i++) {
            myNodeList[i].classList.toggle('list-style');
        }
        checkMarkTitle.innerHTML = (checkMarkTitle.innerHTML === checkMarkLabel.list) ? checkMarkLabel.grid : checkMarkLabel.list;
    }

    addEvents() {
        this.checkbox = document.querySelector('.show-list input');
        this.checkbox.addEventListener('click', this.toggleListStyle.bind(this));
    }

    removeEvents() {
        this.checkbox.removeEventListener('click', this.toggleListStyle.bind(this));
    }

    // TODO: link it to a clean up operation
    onDestroy() {
        super.onDestroy();
        this.removeEvents();
    }
}
