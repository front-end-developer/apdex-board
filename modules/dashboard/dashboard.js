/**
 * Created by Mark Webley on 03/04/2020.
 */
import ApdexServices from '../../services/apdex-services.js';
import HostApplications from './host-applications.js';
import Template from './template.js';

export default class Dashboard extends HostApplications {
    data;
    memoization = [];
    onClickHostApps = [];

    constructor() {
        super();
        this.initialise();
    }

    // Note to developers: where English is not your first language:
    // Please remember:
    //              British spelling: Initialise,
    //              American spelling: Initialize
    async initialise() {
        this.data = await ApdexServices.getInstance().getAll();
        this.render();
    }

    createHostApp(host, data) {
        const appData = {
            "name":         data.name,
            "contributors": data.contributors,
            "version":      data.version,
            "apdex":        data.apdex,
        };

        const hostApplication = this.hostApplications.length && this.hostApplications.find(app => app.host === host);
        if (hostApplication) {
            hostApplication.apps.push(appData);
        } else {
            const hostApp = {
                "host": host,
                "apps": [
                    appData
                ]
            };
            this.hostApplications.push(hostApp)
        }
    };

    createHost(hosts, appData) {
        if (hosts.length === 0) {
            return;
        }
        let domain = hosts.pop();
        if (this.memoization.indexOf(domain) > -1) {
            this.createHostApp(domain, appData);
            return;
        }
        this.memoization.push(domain);
        this.createHostApp(domain, appData);
        this.createHost(hosts, appData);
    }

    render() {
        let tmpl = '';
        this.hostApplications = [];
        for (let i = 0; i < this.data.length; i++) {
            let cloneHosts = [...this.data[i].host];
            this.createHost(cloneHosts, this.data[i]);
        }

        this.testBaseClassMethods();

        this.hostApplications.map((item) => {
            tmpl += this.drawPanelTemplate(item);
        });
        const dashboardContainer = document.querySelector('.dashboard-container');
        dashboardContainer.insertAdjacentHTML('beforeend', tmpl.replace(/\n/mg, ''));

        if (this.onClickHostApps.length) {
            this.removeEvents();
        }
        this.addEvents();
    }

    /**
     * @description     used to test base class methods only can this can be removed:
     *                  getTopAppsByHost | addAppToHosts | removeAppFromHosts
     */
    testBaseClassMethods() {
        this.getTopAppsByHost('e7bf58af-f0be.dallas.biz');
        this.addAppToHosts('e7bf58af-f0be.dallas.biz', {
            "name": "webley webley webley - webley, webley webley, Group",
            "contributors": [
                "Moses",
                "Abraham"
            ],
            "version": 2,
            "apdex": 200,
        });
        this.removeAppFromHosts('e7bf58af-f0be.dallas.biz', "webley webley webley - webley, webley webley, Group");
    }

    toggleHint(e) {
        const hintDisplay = e[0].querySelector('.app-version-number');
        hintDisplay.style.display = 'inline-block';
        setTimeout(() => {
            hintDisplay.style.display = 'none';
        }, 1000);
    }

    addEvents() {
        this.onClickHostApps = document.querySelectorAll('.host-application');
        for (var i = 0; i < this.onClickHostApps.length; i++) {
            const appName = this.onClickHostApps[i].getAttribute('data-app');
            this.onClickHostApps[i].addEventListener('click', this.toggleHint.bind(this, [this.onClickHostApps[i], appName]));
        }
    }


    removeEvents() {
        this.onClickHostApps.removeEventListener('click', this.toggleHint.bind(this));
        for (var i = 0; i < this.onClickHostApps.length; i++) {
            this.onClickHostApps[i].removeEventListener('click', this.toggleHint.bind(this));
        }
    }


    onDestroy() {
        super.onDestroy();
        this.removeEvents();
    }

    drawPanelTemplate(tmpl) {
        let appsTemplate = '';
        let numberOfApps = 0;
        let maxNumberOfApps = 5;

        tmpl.apps.sort((prev, next) => {
            return next.apdex - prev.apdex;
        });

        for (let app of tmpl.apps) {
            numberOfApps++;
            if (numberOfApps > maxNumberOfApps) {
                break;
            }
            appsTemplate += this.drawAppTemplate(app).replace(/\n/mg, '');
        }

        return Template.panel(tmpl, appsTemplate);
    }

    drawAppTemplate(app) {
        return Template.apps(app);
    }
}
