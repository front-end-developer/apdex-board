/**
 * Created by Mark Webley on 05/04/2020.
 */
let memoization = [];
function setHosts(hosts) {
    if (hosts.length === 0) {
        return;
    }
    let domain = hosts.pop();
    if (memoization.indexOf(domain) > -1) {
        return memoization[memoization.indexOf(domain)];
    }

    memoization.push(domain);
    setHosts(hosts);
}

hosts = [
    "e7bf58af-f0be.dallas.biz",
    "e7bf58af-f0be.dallas.biz",
    "e7bf58af-f0be.dallas.biz",
    "b0b655c5-928a.nadia.biz",
    "95b346a0-17f4.abbigail.name"
];

setHosts(hosts);

___________

hostApp.host = host;
hostApp.apps = [];
hostApp.apps.push(appData);

___________

for (let i = 0; i < responseData.length; i++) {
    let cloneHosts = [...responseData[i].host];
    createHost(cloneHosts, responseData[i]);
}
___________

// tmpl.apps.
sortedAppsByApdex.forEach((app) => {
    appsTemplate += this.applyTemplate(app).replace(/\n/mg, '');
});

___________

hostApp.apps.forEach((app, index) => {
    if (app.name === appName) {
        console.log(JSON.stringify(hostApp.apps[index], null, 7));
        console.log('before', hostApp.apps);
        delete hostApp.apps[index];
        console.log('after', hostApp.apps);
        // break
    }
});
___________

___________

___________

const responseData = [
    {
        "name": "1) Mark Webley - Kautzer - Boyer, and Sons",
        "contributors": [
            "Edwin Reinger",
            "Ofelia Dickens",
            "Hilbert Cole",
            "Helen Kuphal",
            "Maurine McDermott Sr."
        ],
        "version": 7,
        "apdex": 68,
        "host": [
            "7e6272f7-098e.dakota.biz",
            "9a450527-cdd9.kareem.info",
            "e7bf58af-f0be.dallas.biz"
        ]
    },
    {
        "name": "2) Mark Webley - Kautzer - Boyer, and Sons",
        "contributors": [
            "Edwin Reinger",
            "Ofelia Dickens",
            "Hilbert Cole",
            "Helen Kuphal",
            "Maurine McDermott Sr."
        ],
        "version": 7,
        "apdex": 68,
        "host": [
            "7e6272f7-098e.dakota.biz",
            "9a450527-cdd9.kareem.info",
            "e7bf58af-f0be.dallas.biz"
        ]
    },
    {
        "name": "Small Fresh Pants - Kautzer - Boyer, and Sons",
        "contributors": [
            "Edwin Reinger",
            "Ofelia Dickens",
            "Hilbert Cole",
            "Helen Kuphal",
            "Maurine McDermott Sr."
        ],
        "version": 7,
        "apdex": 68,
        "host": [
            "7e6272f7-098e.dakota.biz",
            "9a450527-cdd9.kareem.info",
            "e7bf58af-f0be.dallas.biz"
        ]
    },
    {
        "name": "Mark Webley - Kautzer - Boyer, and Sons",
        "contributors": [
            "Edwin Reinger",
            "Ofelia Dickens",
            "Hilbert Cole",
            "Helen Kuphal",
            "Maurine McDermott Sr."
        ],
        "version": 7,
        "apdex": 68,
        "host": [
            "7e6272f7-098e.dakota.biz",
            "9a450527-cdd9.kareem.info",
            "e7bf58af-f0be.dallas.biz"
        ]
    },
    {
        "name": "Refined Concrete Shirt - Hudson - Sauer, Group",
        "contributors": [
            "Ramon Harris DDS",
            "Summer Dicki",
            "Triston Sipes",
            "Fae Lind",
            "Carole Emard"
        ],
        "version": 6,
        "apdex": 57,
        "host": [
            "e0419f48-6a5a.craig.info",
            "7e6272f7-098e.dakota.biz"
        ]
    },
    {
        "name": "Ergonomic Wooden Soap - Lemke and Sons, Inc",
        "contributors": [
            "Miss Moises Walter",
            "Caroline Murazik"
        ],
        "version": 2,
        "apdex": 61,
        "host": [
            "e7bf58af-f0be.dallas.biz",
            "b0b655c5-928a.nadia.biz",
            "95b346a0-17f4.abbigail.name"
        ]
    }
];
___________

const app = {
    "host": "e7bf58af-f0be.dallas.biz",
    "apps": [
        {
            "name": "Ergonomic Wooden Soap - Lemke and Sons, Inc",
            "contributors": [
                "Miss Moises Walter",
                "Caroline Murazik"
            ],
            "version": 2,
            "apdex": 61,
        },
        {
            "name": "Ergonomic Wooden Soap - Lemke and Sons, Inc",
            "contributors": [
                "Miss Moises Walter",
                "Caroline Murazik"
            ],
            "version": 2,
            "apdex": 60,
        }
    ]
};

___________


// TODO: convert into a clas
async function getAll() {
    // toggleLoader(true);
    try {
        // 'http://www.wisdomor.co.uk/mocks/host-app-data-small-data.json'
        const res = await fetch(API.HOSTS);
        const data = await res.json();
        const test = console.log(data);
        // toggleLoader(false);
    } catch(err) {
        console.error('Error: on save: ', err.message);
        // toggleLoader(false);
    }
}

___________

<!-- <script src="index.js" type="module"></script> -->
___________

css module version:
    import cssModule from './dashboard.module.scss';

    static panel(tmpl, appsTemplate) {
    return `
            <div class="${cssModule.panel}">
                <section>
                    <h3>${tmpl.host}</h3>
                     ${appsTemplate}
                 </section>
            </div>
            `;
}

static apps(app) {
    return `
        <div class="">
            <span class="${cssModule.apdex}">${app.apdex}</span>
            <span class=${cssModule.apdexName}">${app.name}</span>
        </div>`;
}
___________

___________

___________

___________

___________

/**
 * @hostName        hostname
 * @appData         {
         *                      "name": "Ergonomic Wooden Soap - Lemke and Sons, Inc",
         *                      "contributors": [
         *                           "Miss Moises Walter",
         *                           "Caroline Murazik"
         *                        ],
         *                       "version": 2,
         *                       "apdex": 61,
         *                    }
 * @description    update the list of applications with higher Apdex

 */
const addAppToHosts = (hostName, appData) => {
    if (!validateAppObject(appData)) {
        return;
    }
    if (!validateHostName(hostName)) {
        throw new Error('invalid hostname');
        return;
    }
    this.hostApplications.find((domain) => domain.host === hostName).apps.push(appData);
    getTopAppsByHost(hostName);
}

___________

// TODO:
// - list applications running on every "host"
// - application has, consists of:
//                      "name", "contributors", ( release version number ) "version",
//                      list of list of "hosts" that the app has been deployed at.

// - "apdex":   Each application has an Apdex metric assigned:
//                                                  0 (Frustrated) and 100 (Satisfied) | (Apdex score is a positive integer)
//
// -

const validateHostName = (hostName) => {
    var regExp = new RegExp('^([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?');
    return regExp.test(hostName);
}

const validateAppObject = (app) => {
    let valid = true;
    const errorMessage = 'Error: app object is missing ';
    const appProperties = {
        name: 'name',
        contributors: 'contributors',
        version: 'version',
        apdex: 'apdex'
    };
    if (typeof app === 'undefined' || app === null) {
        throw new Error('app data not provided');
    }

    if (Object.keys(app).indexOf(appProperties.name) == -1) {
        valid = false;
        throw new Error(`${errorMessage} ${appProperties.name}`);
    }
    if (Object.keys(app).indexOf(appProperties.contributors) == -1) {
        valid = false;
        throw new Error(`${errorMessage} ${appProperties.contributors}`);
    }
    if (Object.keys(app).indexOf(appProperties.version) == -1) {
        valid = false;
        throw new Error(`${errorMessage} ${appProperties.version}`);
    }
    if (Object.keys(app).indexOf(appProperties.apdex) == -1) {
        valid = false;
        throw new Error(`${errorMessage} ${appProperties.apdex}`);
    }
    return valid;
}

___________

const createHost = (hosts, appData) => {
    if (hosts.length === 0) {
        return;
    }
    let domain = hosts.pop();
    if (memoization.indexOf(domain) > -1) {
        this.createHostApp(domain, appData);
        return; // memoization[memoization.indexOf(domain)];
    }
    memoization.push(domain);
    this.createHostApp(domain, appData);
    createHost(hosts, appData);
}
___________

TODO:

- tests
/**
 * @description    update the list of applications with higher Apdex
 */
const removeAppFromHosts = (hostName, appName) => {
    if (!validateHostName(hostName)) {
        throw new Error('invalid hostname');
        return;
    }
    if (typeof appName === 'undefined' || appName === null) {
        throw new Error('invalid app name');
        return;
    }

    const hostApp = this.hostApplications.find((domain) => domain.host === hostName);
    let index = 0;
    for (let app of hostApp.apps) {
        if (app.name === appName) {
            delete hostApp.apps[index];
            break;
        }
        index++;
    }
    getTopAppsByHost(hostName);
}


/**
 * @description    method that, given a hostname, retrieves a
 *                  list of the 25 most satisfying applications.
 */
const getTopAppsByHost = (hostName) => {
    if (!validateHostName(hostName)) {
        throw new Error('invalid hostname');
        return;
    }
    let topItems = '';
    let numberOfApps = 0;
    let maxNumberOfApps = 25;
    const hostObject = this.hostApplications.find((domain) => domain.host === hostName);
    hostObject.apps.sort((prev, next) => {
        return next.apdex - prev.apdex;
    });

    const copyHostObject = JSON.parse(JSON.stringify(hostObject));
    copyHostObject.apps.splice(25);
    return copyHostObject;
}



// TODO: Unit Test
    this.addAppToHosts('e7bf58af-f0be.dallas.biz', {
        "name": "webley webley webley",
        "contributors": [
            "Moses",
            "Abraham"
        ],
        "version": 2,
        "apdex": 200,
    });

___________

<section class="dashboard-container">
    <div class="panel">
    <section>
    <h3>domain.biz</h3>
    <div class="">
    <span class="apdex">99</span>
    <span class="apdex-name">Licensed Cotton Cair - Theil, Graham and Mraz, LLC</span>
</div>

<div class="">
    <span class="apdex">99</span>
    <span class="apdex-name">Licensed Cotton Cair - Theil, Graham and Mraz, LLC</span>
</div>

<div class="">
    <span class="apdex">99</span>
    <span class="apdex-name">Licensed Cotton Cair - Theil, Graham and Mraz, LLC</span>
</div>

<div class="">
    <span class="apdex">99</span>
    <span class="apdex-name">Licensed Cotton Cair - Theil, Graham and Mraz, LLC</span>
</div>

<div class="">
    <span class="apdex">99</span>
    <span class="apdex-name">Licensed Cotton Cair - Theil, Graham and Mraz, LLC</span>
</div>
</section>
</div>
</section>

_______________
index.html (js class modules):
    <script src="index.js" type="module"></script>
