/**
 * Created by Mark Webley on 07/04/2020.
 */

const path = require('path');

module.exports = function(config) {
    config.set({

        // anything named karma-* is normally auto included so you probably dont need this
        plugins: [
            require('webpack'),
            require('karma-webpack'),
            require('webpack-dev-middleware'),
            require('karma-sourcemap-loader'),

            require('karma-jasmine'),
            require('karma-chrome-launcher'),
            require('karma-ie-launcher'),
            require('karma-firefox-launcher'),
            require('karma-jasmine'),
            require('karma-coverage'),
            require('karma-jasmine-html-reporter'),
            require('karma-coverage-istanbul-reporter')
        ],

        // base path that will be used to resolve all patterns (eg. files, exclude)
        basePath: '',


        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: [
            'jasmine'
        ],


        // list of files / patterns to load in the browser
        files: [
            {   // TODO: delete just tying to debug and text. fix karma, jasmine test SUIT
                pattern: 'test/*.spec.js',
                type: 'module',
                included: false, // if true CAUSES:  SyntaxError: Cannot use import statement outside a module
                watched: true
            },
            {
                pattern: 'modules/common/**/test/*.spec.js',
                type: 'module',
                included: false,
                watched: true
            },
            {
                pattern: 'modules/**/test/*.spec.js',
                type: 'module',
                included: false,
                watched: true
            }
        ],


        // list of files / patterns to exclude
        exclude: [
        ],


        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: {
            // add webpack as preprocessor
            'modules/common/**/test/*.spec.js': ['sourcemap'], // 'webpack',
            'modules/**/test/*.spec.js': ['sourcemap'] // 'webpack',
        },

        webpackMiddleware: {
            // webpack-dev-middleware configuration
            // i. e.
            stats: 'errors-only'
        },

        // test results reporter to use
        // possible values: 'dots', 'progress'
        // available reporters: https://npmjs.org/browse/keyword/karma-reporter
        reporters: ['coverage-istanbul', 'Chrome_without_security', 'progress', 'kjhtml'],


        // web server port
        port: 9876,


        // enable / disable colors in the output (reporters and logs)
        colors: true,


        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,


        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,


        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        browsers: ['Chrome'], // ['Chrome', 'IE', 'Firefox'],


        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        singleRun: false,

        // Concurrency level
        // how many browser should be started simultaneous
        concurrency: Infinity,

        // you can define custom flags
        customLaunchers: {
            Chrome_without_security: {
                base: 'Chrome',
                flags: ['--disable-web-security', '--disable-site-isolation-trials']
            }
        },

        // any of these options are valid: https://github.com/istanbuljs/istanbuljs/blob/aae256fb8b9a3d19414dcf069c592e88712c32c6/packages/istanbul-api/lib/config.js#L33-L39
        coverageIstanbulReporter: {
            // reports can be any that are listed here: https://github.com/istanbuljs/istanbuljs/tree/aae256fb8b9a3d19414dcf069c592e88712c32c6/packages/istanbul-reports/lib
            reports: ['html', 'lcovonly', 'text-summary'],

            // base output directory. If you include %browser% in the path it will be replaced with the karma browser name
            dir: path.join(__dirname, 'coverage'),

            // Combines coverage information from multiple browsers into one report rather than outputting a report
            // for each browser.
            combineBrowserReports: true,

            // if using webpack and pre-loaders, work around webpack breaking the source path
            fixWebpackSourcePaths: true,

            // Omit files with no statements, no functions and no branches from the report
            skipFilesWithNoCoverage: true,

            // Most reporters accept additional config options. You can pass these through the `report-config` option
            'report-config': {
                // all options available at: https://github.com/istanbuljs/istanbuljs/blob/aae256fb8b9a3d19414dcf069c592e88712c32c6/packages/istanbul-reports/lib/html/index.js#L135-L137
                html: {
                    // outputs the report in ./coverage/html
                    subdir: 'html'
                }
            },

            // enforce percentage thresholds
            // anything under these percentages will cause karma to fail with an exit code of 1 if not running in watch mode
            thresholds: {
                emitWarning: false, // set to `true` to not fail the test command when thresholds are not met
                // thresholds for all files
                global: {
                    statements: 100,
                    lines: 100,
                    branches: 100,
                    functions: 100
                },
                // thresholds per file
                each: {
                    statements: 100,
                    lines: 100,
                    branches: 100,
                    functions: 100,
                    overrides: {
                        'baz/component/**/*.js': {
                            statements: 98
                        }
                    }
                }
            },

            verbose: true, // output config used by istanbul for debugging

            // `instrumentation` is used to configure Istanbul API package.
            instrumentation: {
                // To include `node_modules` code in the report.
                'default-excludes': false
            }
        }
    })
};
